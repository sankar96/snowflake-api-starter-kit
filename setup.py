from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='Snowflake-API-starter-kit',
    version='1.0',
    packages=['config', 'service', 'service.blueprint', 'service.repositories', 'service.sqls', 'service.utils'],
    url='',
    license='MIT License',
    author='Sankara Subramanian',
    author_email='sankar.subramanian96@gmail.com',
    description='Snowflake API Starter kit',
    install_requires=requirements
)
