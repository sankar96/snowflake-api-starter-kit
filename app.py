import json
import os
from logging.config import dictConfig
from flask import Flask
from flask_cors import CORS
from cacheConfig import cache

print(os.path.dirname(__file__))
LOG_CONFIG_PATH = os.path.dirname(__file__) + "/config/logging.json"

with open(LOG_CONFIG_PATH) as log_config_file:
    log_config = json.load(log_config_file)

dictConfig(config=log_config)

# TODO: Add in  mail loggers. Add in logger from other libraries. refer: https://flask.palletsprojects.com/en/1.1.x/logging/

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
cache.init_app(app)

from service.blueprints import salary_info

app.register_blueprint(salary_info.salary_info_bp)
app.run(debug=True)
