from cachetools import TTLCache, cached
from cryptography.fernet import Fernet
from flask import current_app

from service.utils import configHandler

# TODO: Fetch App secret from AWS Vault.
@cached(cache=TTLCache(maxsize=1024, ttl=400))
def get_app_secret():
    current_app.logger.debug("Accessing Application config to read APP SECRET")
    app_config = configHandler.get_application_config()
    current_app.logger.debug("APP Secret: {}".format(app_config["security"]["app_secret"]))
    return app_config["security"]["app_secret"].encode('utf-8')


# TODO: add encryption type (HMAC) for passwords. refer: https://cryptography.io/en/latest/fernet/
def encrypt(data):
    try:
        fernet = Fernet(get_app_secret())
        return fernet.encrypt(data)
    except:
        current_app.logger.error("Error while encrypting data")


def decrypt(encrypted_data):
    try:
        fernet = Fernet(get_app_secret())
        return fernet.decrypt(encrypted_data).decode("utf-8")
    except:
        current_app.logger.error("Error while decrypting data.")

