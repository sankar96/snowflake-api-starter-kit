import logging
from logging.handlers import SMTPHandler
from service.utils import configHandler

app_config = configHandler.get_application_config()


def get_mail_handler():
    mail_handler = SMTPHandler(
        mailhost=app_config["mailing"]["host"],
        fromaddr=app_config["mailing"]["from"],
        toaddrs=app_config["mailing"]["to_addr"]
    )

    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(logging.Formatter(
        '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
    ))

    return mail_handler
