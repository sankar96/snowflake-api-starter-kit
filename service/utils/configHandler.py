import json
import os

from cachetools import cached, LRUCache
from flask import current_app
import sys

APP_CONFIG_FILE_PATH = os.path.dirname(__file__) + "/../../config/application_config.json"


@cached(cache=LRUCache(maxsize=1024))
def get_application_config():
    try:
        with open(APP_CONFIG_FILE_PATH, 'r') as json_file:
            data = json.load(json_file)
            return data
    except IOError:
        current_app.logger.error("Could not find/open Application config file: {}".format(APP_CONFIG_FILE_PATH))
        sys.exit()
