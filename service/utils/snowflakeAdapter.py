import os
from cachetools import LRUCache, cached
from flask import current_app

from service.utils import configHandler
from service.utils import encryption

import snowflake.connector as sfConnector

from snowflake.connector import DictCursor

from snowflake.connector import errors

# Convert into OBJECT
# TODO: fetch secrets and db credentials from a VAULT
app_config = configHandler.get_application_config()

db_connection_cache = LRUCache(maxsize=2048)
db_cursor_cache = LRUCache(maxsize=2048)

global_default_connection = None

global_default_cursor = None


@cached(cache=db_connection_cache)
def db_connect(account=None):
    if account is None:
        account = app_config["databases"]["default"]
    try:
        connection = sfConnector.connect(
            account=app_config["databases"][account]["account"],
            region=app_config["databases"][account]["region"],
            database=app_config["databases"][account]["database"],
            schema=app_config["databases"][account]["schema"],
            role=app_config["databases"][account]["role"],
            warehouse=app_config["databases"][account]["warehouse"],
            user=app_config["databases"][account]["username"],
            password=encryption.decrypt(app_config["databases"][account]["password"].encode("utf-8"))
        )
    except Exception as err:
        current_app.logger.error(err)
        return {
            "success": False,
            "error": err
        }
    if account == app_config["databases"]["default"]:
        global global_default_connection
        global_default_connection = connection

    return {
        "success": True,
        "connection": connection
    }


@cached(cache=db_cursor_cache)
def get_cursor(db_connection=None):
    if db_connection is None:
        connector = db_connect()
        if (connector["success"]):
            cursor = connector["connection"].cursor(DictCursor)
            global_default_cursor = cursor
            return cursor
    else:
        return db_connection.cursor(DictCursor)


def close_db_connection(connection):
    connection.close()


def get_sql_path(sql_file_path):
    return os.path.dirname(__file__) + "/../sqls/{}".format(sql_file_path)


def replace_db_and_schema_names(query_string, account=None):
    if account is None:
        account = app_config["databases"]["default"]
    return query_string.replace("${SCHEMA_NAME}", app_config["databases"][account]["schema"]).replace("${DB_NAME}",
                                                                        app_config["databases"][account]["database"])


# def commit_changes(connection, cursor):
#     connection.close()
#     cursor.close()


def execute_sql_file(sql_file_name,account=None, fetch=True):
    cursor = get_cursor()
    sql_file_path = get_sql_path(sql_file_name)
    response = []
    try:
        sql_file = open(sql_file_path, 'r')
        query_text = sql_file.read()
        if not query_text:
            raise Exception("SQL File is empty")
        query = replace_db_and_schema_names(query_text,account=account)
        current_app.logger.debug("Query: {}".format(query))
        cursor.execute(query)
        if fetch:
            response = cursor.fetchall()
    except IOError as err:
        current_app.logger.exception(f"SQL file empty: {sql_file_path}. Error: {err}")
    except FileNotFoundError:
        current_app.logger.exception(f"SQL file Cannot be found: {sql_file_path}")
    except Exception as err:
        current_app.logger.exception(
            f"Exception while executing the query file: {sql_file_path}.\n Stacktrace: {err} \n")
    finally:
        global global_default_connection
        global global_default_cursor
        # commit_changes(global_default_connection, global_default_cursor)

    return response


def execute_param_query(sql_file_name, parameters,account=None, fetch=True):
    cursor = get_cursor()
    sql_file_path = get_sql_path(sql_file_name)
    response = []
    try:
        sql_file = open(sql_file_path, 'r')
        query_text = sql_file.read()
        if not query_text:
            raise Exception("SQL File is empty")
        query = replace_db_and_schema_names(query_text,account=account)
        query = query.format(*parameters)
        current_app.logger.debug("Query: {}".format(query))
        cursor.execute(query)
        if fetch:
            response = cursor.fetchall()

    except (IOError, Exception):
        current_app.logger.exception("SQL file empty: {}".format(sql_file_path))
    except FileNotFoundError:
        current_app.logger.exception("SQL file Cannot be found: {}".format(sql_file_path))
    except:
        current_app.logger.exception(
            "Exception while executing the query file: {}.\n Stacktrace: \n".format(sql_file_path))
    finally:
        global global_connection
        global global_cursor
        # commit_changes(global_connection, global_cursor)
    return response


# TODO: To Check Functionality
def execute_query(query_text,account=None, fetch=True):
    cursor = get_cursor()
    response = []
    try:
        query = replace_db_and_schema_names(query_text, account=account)
        cursor.execute(query)
        if fetch:
            response = cursor.fetchall()
    except:
        current_app.logger.exception("Exception while executing query: {} \n Stacktrace: \n".format(query))
    return response
