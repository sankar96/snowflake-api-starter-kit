from ..utils import snowflakeAdapter
from flask import current_app
import sys
from service.utils import configHandler

app_config = configHandler.get_application_config()


def get_all_salary_info():
    current_app.logger.debug("Fetching all salary info")

    salary_info = []
    account = app_config["databases"]["default"]
    try:
        salary_info = snowflakeAdapter.execute_sql_file("salary/get_all_salary_info.sql",account=account)
    except:
        current_app.logger.exception("Error While fetching all salary info")

    return salary_info


def insert_new_salary_info(serial_no, userid, gender, age, estimatedsal, purchased, recordtype):
    current_app.logger.debug("Fetching all salary info")

    salary_info = []
    account = app_config["databases"]["default"]

    try:
        salary_info = snowflakeAdapter.execute_sql_file("salary/insert_new_salary_info.sql",
                                                        [serial_no, userid, gender, age, estimatedsal, purchased,
                                                         recordtype],account=account)
    except:
        current_app.logger.exception("Error While fetching all salary info")

    return salary_info
