from flask_cors import cross_origin
from flask import current_app, Blueprint, jsonify, request
import jsonschema
# from ...cacheConfig import cache
from ..repositories import salaryRepository

salary_info_bp = Blueprint("department", __name__, url_prefix="/api")

salary_info_schema = {
    "type": "object",
    "properties": {
        "serialNo": {
            "type": "integer"
        },
        "userId": {
            "type": "integer"
        },
        "gender": {
            "type": "string"
        },
        "age": {
            "type": "integer"
        },
        "estimatedSal": {
            "type": "integer"
        },
        "recordType": {
            "type": "string"
        }
    },
    "additionalProperties": False,
    "required": ["serialNo", "userId", "gender", "age", "estimatedSal", "purchased", "recordType"]
}


# @cache.cached(timeout=500)
@salary_info_bp.route("/salary_info", methods=["GET"])
def get_all_salary_info():
    salary_info = salaryRepository.get_all_salary_info()
    current_app.logger.debug("department ID: {}".format(salary_info))
    return jsonify(salary_info)


@salary_info_bp.route("/salary_info", methods=["POST"])
def create_new_salary_info():
    payload = request.get_json()
    try:
        jsonschema.validate(instance=payload, schema=salary_info_schema)
        current_app.logger.info(f"Schema Validation Complete")
    except jsonschema.exceptions.ValidationError as error:
        return jsonify({
            "error": f"Validation Error: {error}"
        })
    return jsonify(salaryRepository.insert_new_salary_info(payload["serialNo"], payload["userId"], payload["gender"],
                                                           payload["age"], payload["estimatedSal"],
                                                           payload["purchased"], payload["recordType"]))
